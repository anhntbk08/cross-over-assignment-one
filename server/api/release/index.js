'use strict';

var express = require('express');
var controller = require('./release.controller');
var auth = require('../../auth/auth.service');
var router = express.Router();

router.get('/', auth.hasRole('admin'), controller.index);
router.post('/changelist', auth.hasRole('admin'), controller.createChangelist);
router.post('/mergeBuild', auth.hasRole('admin'), controller.mergeBuild);
router.get('/checkRunningRelease', auth.hasRole('admin'), controller.checkRunningRelease);
router.put('/triggerTest', auth.hasRole('admin'), controller.triggerTest);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);

module.exports = router;