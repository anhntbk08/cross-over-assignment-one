'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var ReleaseSchema = new Schema({
  name: String,
  isBuild: Boolean,
  active: Boolean,
  owner: {
    type: mongoose.Schema.ObjectId,
    ref: 'User'
  },
  startedTime: Date,
  status: Number,
  metrics: Schema.Types.Mixed,
  build: Schema.Types.Mixed,
  unitTest: Schema.Types.Mixed,
  functionalTest: Schema.Types.Mixed
});

module.exports = mongoose.model('Release', ReleaseSchema);