'use strict';

require('mongoose-pagination');
var _ = require('lodash');
var Release = require('./release.model');
var User = require('../user/user.model');

var RELEASE_STATUS_CONSTANT = {
  ACCEPTED: 1,
  COMPLETE: 2,
  REJECTED: 3,
  RUNNING: 4,
  PENDING: 5
}

// Get list of releases
exports.index = function(req, res) {
  var currentPage = req.query.page || 1;
  var pageSize = req.query.pageSize || 10;

  Release
    .find({})
    .populate({
      path: "owner",
      select: "name -_id"
    })
    .sort({ _id: -1 })
    .paginate(currentPage, pageSize, function (err, releases, totalItems) {
      if(err) { return handleError(res, err); }
      return res.json(200, {success: true, data: {releases: releases, totalItems: totalItems}});
    });
    
};

// check any running release
exports.checkRunningRelease = function(req, res){
  Release
    .find({status: RELEASE_STATUS_CONSTANT.RUNNING})
    .exec(function (err, releases) {
      if(err) { return handleError(res, err); }

      if (releases.length)
        return res.json(200, {success: false});
      else
        return res.json(200, {success: true});
    });
}


/*
  util to update single release with input param
*/
function updateRelease(id, data, callback){
  Release
  .update(
  {
    _id: id
  },
  {
    $set: data
  },
  {}, 
  function(error, result){
    if (callback)
      callback(error, result)
  });
}


/*
  
*/

function randomInteger(){

}
/*
  util to generate data for a release
  data includes
    - metrics
    - build
    - unitTest
    - functionalTest
*/
function generateReleaseData(){
  var booleanArr = [true, false];

  function randomizeStatus (flag){
    if (flag)
      return flag;
    else
      return booleanArr[_.random(0, 1)]
  }

  // random the release status
  var releaseStatus = booleanArr[_.random(0, 1)];
  return {
    metrics: {
      isTestSuccess: randomizeStatus(releaseStatus),
      isMaintainable: randomizeStatus(releaseStatus),
      testPercentage: _.random(10, 100),
      maintainabilityPercentage: _.random(10, 100),
      security: _.random(10, 100),
      workmanship: _.random(10, 100)
    },
    build: {
      isPassTest: randomizeStatus(releaseStatus),
      testedTime: new Date()
    },
    unitTest: {
      isRunnableTest: randomizeStatus(releaseStatus),
      testType: "Unit",
      percentagePassed: _.random(10, 100),
      testNumber: _.random(50, 200)
    },
    functionalTest: {
      isRunnableTest: randomizeStatus(releaseStatus),
      testType: "Functional",
      percentagePassed: _.random(10, 100),
      testNumber: _.random(200, 2000)
    }
  };
};



// trigger test for a release
// I fakes the test result in range 10 seconds
exports.triggerTest = function(req, res){
  var releaseId = req.body.id;
  var isBuild = req.body.isBuild;

  // update timeStarted and change status to running
  updateRelease(releaseId, {
    startedTime: new Date(), 
    status: RELEASE_STATUS_CONSTANT.RUNNING
  }, function(error, result){
    console.log(result);
  })

  
  /*
    Simulate a testing progress by randomizing a time between 0 and 10s
    after that return the result of running test
  */
  var timeout = Math.round( Math.random()*10 )*1000;
  
  setTimeout(function(){
    var updatedData = generateReleaseData();

    /*
      If Every test is pass the status should be complete or accepted
      else the status should be rejected
    */
    if (updatedData.metrics.isTestSuccess &&
        updatedData.metrics.isMaintainable &&
        updatedData.build.isPassTest &&
        updatedData.unitTest.isRunnableTest &&
        updatedData.functionalTest.isRunnableTest){
      if (isBuild){
        updatedData.status = RELEASE_STATUS_CONSTANT.COMPLETE;
      }
      else{
        updatedData.status = RELEASE_STATUS_CONSTANT.ACCEPTED;
      }
    }
    else{
      if (isBuild)
        // remove it directly from list
        Release.findById(releaseId, function(err, release){
          if(error) { return handleError(res, error); }
          if (!release)
            return res.json(200, {success: false, message: "Can't find info of the release, it maybe removed!"});

          release.remove(function(error, result){
            if(error) { return handleError(res, error); }

            return res.json(200, {success: true, message: "The Build was rejected so it will be removed from the list !"});
          });
        });
      else
        updatedData.status = RELEASE_STATUS_CONSTANT.REJECTED;
    }
    

    updateRelease(releaseId, updatedData, function(error, result){
      if(error) { return handleError(res, error); }

      return res.json(200, {success: true});
    });
    
  }, timeout);
}


/*
  Merge a changelist into current build
  what exactly we do is remove the changelist and add new build
*/
exports.mergeBuild = function(req, res) {
  Release.findById(req.body.id).exec(function(err, release){
    if(err) { return handleError(res, err); }

    if (release){
      var build = {
        name: "build_" + release.name.split("_")[1],
        isBuild: true,
        owner: req.user,
        status: RELEASE_STATUS_CONSTANT.PENDING
      };
      release.remove();

      Release.create(build, function(err, release) {
        if(err) { return handleError(res, err); }
        return res.json(201, release);
      });
    }
    
  });
  
};

// Get a single release
exports.show = function(req, res) {
  Release.findById(req.params.id, function (err, release) {
    if(err) { return handleError(res, err); }
    if(!release) { return res.send(404); }
    return res.json(release);
  });
};

// Creates a new release in the DB.
exports.createChangelist = function(req, res) {
  var changelist = {
    name: "changelist_" + (new Date()).valueOf(),
    isBuild: false,
    owner: req.user,
    status: RELEASE_STATUS_CONSTANT.PENDING
  }
  Release.create(changelist, function(err, release) {
    if(err) { return handleError(res, err); }
    return res.json(201, release);
  });
};

// Updates an existing release in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Release.findById(req.params.id, function (err, release) {
    if (err) { return handleError(res, err); }
    if(!release) { return res.send(404); }
    var updated = _.merge(release, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, release);
    });
  });
};

// Deletes a release from the DB.
exports.destroy = function(req, res) {
  Release.findById(req.params.id, function (err, release) {
    if(err) { return handleError(res, err); }
    if(!release) { return res.send(404); }
    release.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}