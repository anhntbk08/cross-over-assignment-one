'use strict'

angular.module 'assignmentOneApp'

.constant "RELEASE_STATUS_CONSTANT",
  ACCEPTED: 1
  COMPLETE: 2
  REJECTED: 3
  RUNNING: 4
  PENDING: 5

.constant "RELEASE_STATUS",
  [
    { 
      id: 0
      text: "Default"
      class: ""
    },
    {
      id: 1
      text: "Accepted"
      class: "accepted"
    },
    {
      id: 2
      text: "Complete"
      class: "complete"
    },
    {
      id: 3
      text: "Rejected"
      class: "rejected"
    },
    {
      id: 4
      text: "Running"
      class: "running"
    },
    {
      id: 5
      text: "Pending"
      class: "pending"
    }
  ]

.controller 'AdminCtrl', [
  "$scope",
  "$http",
  "Auth",
  "User",
  "RELEASE_STATUS",
  "RELEASE_STATUS_CONSTANT"
  "Release"
  ($scope, $http, Auth, User, RELEASE_STATUS, RELEASE_STATUS_CONSTANT, Release) ->
    $scope.RELEASE_STATUS_CONSTANT = RELEASE_STATUS_CONSTANT

    DEFAULT_TEST_DATA =
      metrics:
        isTestSuccess: false
        isMaintainable: false
        testPercentage: 0
        maintainabilityPercentage: 0
        security: 0
        workmanship: 0
      build:
        isPassTest: false
        testedTime: " "
      unitTest:
        isRunnableTest: false
        testType: "Unit"
        percentagePassed: 0
      functionalTest:
        isRunnableTest: false
        testType: "Functional"
        percentagePassed: 0

    ###
      release list status
      TODO add more filter : Name, status
    ###
    $scope.releaseListInformation =
      pageSize: 10
      page: 1
      totalItems: 0

    updateStatusForRelease = (status, release)->
      release.status = status
      statusInfo = RELEASE_STATUS.getByObjectId status
      if statusInfo
        release.statusText = statusInfo.text
        release.classDOM = statusInfo.class

    # set full progress of each test kind
    # calculate the proper background color - this will avoid watch and doing on $apply angularjs
    setBlockClassDOMforRelease = (release)->
      if  release.status isnt RELEASE_STATUS_CONSTANT.RUNNING and
          release.status isnt RELEASE_STATUS_CONSTANT.PENDING 
        release.metrics.progress = 100
        release.metrics.classDOM = if release.metrics.isTestSuccess && release.metrics.isMaintainable then "bg-success" else "bg-danger"

        release.build.progress = 100
        release.build.classDOM = if release.build.isPassTest then "bg-success" else "bg-danger"

        release.unitTest.progress = 100
        release.unitTest.classDOM = if release.unitTest.isRunnableTest then "bg-success" else "bg-danger"

        release.functionalTest.progress = 100
        release.functionalTest.classDOM = if release.functionalTest.isRunnableTest then "bg-success" else "bg-danger"

    findErrorsOfRelease = (release)->
      errorsText = ""
      return if !release.metrics
      if !release.metrics.isTestSuccess || !release.metrics.isMaintainable
        errorsText += "Metrics reduction";
      else if !release.build.isPassTest
        errorsText += ", Build failed";
      else if !release.unitTest.isRunnableTest
        errorsText += ", Can't run unit test";
      else if !release.functionalTest.isRunnableTest
        errorsText += ", Can't run functional test";

      errorsText = $.trim(errorsText);
      if errorsText[0] && errorsText[0] == ","
        errorsText = errorsText.replace ",", ""

      release.errorsText = errorsText


    # query release list by release pagination information
    $scope.queryReleaseList = ()->
      result = Release
              .get(
                pageSize: $scope.releaseListInformation.pageSize,
                page: $scope.releaseListInformation.page
              )

      result.$promise.then ()->
        $scope.releaseListInformation.totalItems = result.data.totalItems

        $scope.releases = _.each result.data.releases, (release)->
          updateStatusForRelease release.status, release

          if release.startedTime
            release.startedTime = (new Date(release.startedTime)).format("dd/mm/yyyy HH:MM tt")

          if release.build?.testedTime
            release.build.testedTime = (new Date(release.build.testedTime)).format("dd/mm/yyyy HH:MM tt")

          findErrorsOfRelease release
          setBlockClassDOMforRelease release

    $scope.queryReleaseList()


    ###
      Add more changelist function:
      Because the requirement is not clear, i assume that we have a source of changelist
      they will be push to release list when somebody approves (like approve pull request in gitflow)
      In here to easy to create dynamic data, i faked the source from server-side
    ###
    $scope.addNewChangeList = ()->
      Release.createChangelist( ()->
        $scope.queryReleaseList()
      )
    
    ###
      Run a changelist/ Build
    ###
    $scope.runARelease = (release)->
      releaseId = release._id
      isBuild = if release.isBuild then 1 else 0
      #check if any release running
      Release.checkAnyRunningRelease (res)->
        if res.success
          alert "Started running this release !"

          # update status and client-side
          updateStatusForRelease RELEASE_STATUS_CONSTANT.RUNNING, release
          release.startedTime = (new Date()).format("dd/mm/yyyy HH:MM tt")
          release.build = DEFAULT_TEST_DATA.build
          release.unitTest = DEFAULT_TEST_DATA.unitTest
          release.functionalTest = DEFAULT_TEST_DATA.functionalTest
          release.metrics = DEFAULT_TEST_DATA.metrics

          # simulate the test run on server-side
          result = Release.triggerTestForRelease({id: releaseId, isBuild: isBuild})
          result.$promise.then (res)->
            if res.message
              alert res.message
            $scope.queryReleaseList()
        else
          alert "There is a running release, try again later"

    $scope.mergeBuild = (release)->
      result = Release.mergeBuild {id: release._id}
      result.$promise.then (res)->
        alert "Merge Build Success, Now a new Pending Build is added !"
        $scope.releaseListInformation.page = 1
        $scope.queryReleaseList()

    removeRelease = (release, callback)->
      result = Release.remove {controller: release._id}
      result.$promise.then (res)->
        callback()

    ###
      Actually: i remove them out of system
    ###
    $scope.deployToTest = (release)->
      removeRelease release, (res)->
        alert 'Deploy successfully, now the build is removed from list'
        $scope.queryReleaseList()

    $scope.deployToProduction = (release)->
      removeRelease release, (res)->
        alert 'Deploy successfully, now the build is removed from list'
        $scope.queryReleaseList()

    # remove a changelist
    $scope.removeChangelist = (release, $event)->
      $event.stopPropagation()
      $event.preventDefault()
      removeRelease release, (res)->
        alert 'Remove changelist successfully'
        $scope.queryReleaseList()
]