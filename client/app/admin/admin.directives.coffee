'use strict'

angular.module 'assignmentOneApp'
.directive 'uiChart', [()->
  (
    restrict: 'EAM'
    template: '<div></div>'
    replace: true
    scope:
      chartOptions: "="

    link: (scope, elem, attrs) ->

      renderChart = ->
        elem.html ''

        opts = {}
        if !angular.isUndefined(scope.chartOptions)
          opts = scope.chartOptions
          if !angular.isObject(opts)
            throw 'Invalid ui.chart options attribute'
        # $(elem).height(100)
        elem.highcharts opts
        return

      scope.$watch "chartOptions", ()->
        renderChart()

  )]
.directive 'metricsBlockDirective', [()->
  (
    restrict: 'C'
    templateUrl: 'app/admin/directive_templates/metrics-block.html'
    scope:
      metrics: "="
    link: (scope, elem, attrs) ->
  )]

.directive 'buildBlockDirective', [()->
  (
    restrict: 'C'
    templateUrl: 'app/admin/directive_templates/build-block.html'
    scope:
      buildData: "="
    link: (scope, elem, attrs) ->
  )]

.directive 'testBlockDirective', [()->
  (
    restrict: 'C'
    templateUrl: 'app/admin/directive_templates/test-block.html'
    scope:
      testData: "="
    link: (scope, elem, attrs) ->
      if scope.testData
        passedNumber = parseInt scope.testData.testNumber*scope.testData.percentagePassed/100
        failedNumber = scope.testData.testNumber - passedNumber
      else
        passedNumber = 0
        failedNumber = 0
      scope.chartConfig =
        options:
          colors: ['#72ac4d', '#eb7d3b']
          chart:
            backgroundColor: 'rgba(0,0,0,0)'
            plotBorderWidth: null
            plotShadow: false
            type: "pie"
          plotOptions:
            pie:
              allowPointSelect: true
              cursor: 'pointer'
              dataLabels:
                enabled: true
                format: '{y}'
                distance: -20
        series: [
          {
            data: [["Passed Test", passedNumber], ["Failed Test", failedNumber]]
            type: "pie"
          }
        ]
        title:
          text: ''
        size:
          width: "120"
          height: "120"

  )]