'use strict'

angular.module 'assignmentOneApp'
.factory 'Release', ($resource) ->
  $resource '/api/releases/:controller',
    controller: '@_controller'
  ,
    createChangelist:
      method: 'POST'
      params:
        controller: 'changelist'

    checkAnyRunningRelease:
      method: 'GET'
      params:
        controller: 'checkRunningRelease'

    triggerTestForRelease:
      method: 'PUT'
      params:
        controller: 'triggerTest'

    mergeBuild:
      method: 'POST'
      params:
        controller: 'mergeBuild'


